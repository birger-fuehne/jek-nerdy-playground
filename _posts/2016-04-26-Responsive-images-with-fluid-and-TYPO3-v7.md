---
layout: post
title:  "Responsive images in TYPO3 v7 with fluid styled content"
date:   2016-04-02 19:26:55 +0100
author: "Birger Fühne"
tags: 
  - TYPO3
---

<p>I was about starting my first TYPO3 v7 project using fluid_styled_content, and I wondered what the current state of responsive image generation was.</p>
<!--more-->

<p class="typl8-drop-cap">
	After some googling the next logical step was to ask the developers themselves. How you might ask? Simple, register for <a href="https://forger.typo3.org/slack">the slack</a> and choose the right channel. Even without having a question it's a great place to hang around and check the conversations at the end of the day - it's awesome what you pick up and learn on the go.
</p>


Unfortunately the answer was short (but fast!), there's no such thing for fsc yet. Quite surprising for me, because using css_styled_content, it was (and is) possible to define a render method (picture, data-*, srcset,..) and source collection for an image and get the appropriate frontend code. If you'd like to dive deeper into responsive images and css_styled_content, <a href="https://somethingphp.com/responsive-images-typo3/">this article</a> provided an excellent guide.

But it wouldn't be TYPO3 if there were no contributions done already. Other community members chipped in and provided links to solutions that have come up so far. One is the extension <a href="https://github.com/alexanderschnitzler/fluid-styled-responsive-images"><code>fluid_styled_responsive_images</code></a> by Alexander Schnitzler. Simply install it, add the static template, and add a layout key and sourceCollection configuration. Responsive images done.

With one problem. Imagine you have a fairly standard website layout, let's say a full-width hero image, and a two-column content area below, split 1/3 and 2/3:

```
├─────────────────────┤
│       Head          │
├─────────────┬───────┤
│             │       │
│  left       │ right │
│             │       │
└─────────────┴───────┘

```

Now the title images in the head are spanning the entire width, thus their width would ideally be around 1920px (I'm referring to CSS pixels) on large screens, whereas images in the right column would be at around 640px wide. But if you recall the sourceCollection setup, there's no way to adjust the output of the image tag for this scenario: 

```
small {
    width = 400
    mediaQuery = (max-device-width: 400px)
    dataKey = small
    srcset = 400w
    sizes = 50vw
}

large {
    width = 1200
    mediaQuery = (min-device-width: 1000px)
    dataKey = large
    srcset = 800w
    sizes = 100vw
}
```

So even for images in the sidebar, the size would be at 1200px width for larger screens, even though the maximum needed width would be 640x. To adjust for the positioning, I hacked a bit around to have the ImageRenderer respect the column position of the content element that the image is used in. The column-based rendering definitions are set up in the TypoScript setup:

```
tt_content.textmedia.settings.responsive_image_rendering {
    layoutKey = srcset

    sourceCollection {
        colPos.0 {
            small {
                width = 400
                mediaQuery = (max-device-width: 400px)
                dataKey = small
                srcset = 400w
                sizes = 50vw
            }

            large {
                width = 1200
                mediaQuery = (min-device-width: 1000px)
                dataKey = large
                srcset = 800w
                sizes = 100vw
            }
        }

        colPos.2 {
            small {
                width = 100
                mediaQuery = (max-device-width: 400px)
                dataKey = small
                srcset = 400w
            }

            large {
                width = 200
                mediaQuery = (min-device-width: 1000px)
                dataKey = large
                srcset = 800w
            }
        }
    }
}
```

In case no column position could be determined, the renderer falls back to the colPos.0 definition. One could also think about adding an explicit <code>default</code> entry in the sourceCollection.

The full code is <a href="https://github.com/birger-fuehne/fluid-styled-responsive-images">available on Github</a>. 

Caveat: I'm not entirely sure whether the implementation is production-ready, probably not. It works&trade; so far in my little project. And it also *only* works for the srcset layout key, as I don't need any art direction atm.

Update: According to @herzogkienast, there has been some initiative to implement responsive image rendering to fsc, I'm curious to see what will happen and features will get implemented.