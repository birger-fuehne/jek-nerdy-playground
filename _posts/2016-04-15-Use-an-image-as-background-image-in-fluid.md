---
layout: post
title:  "Use an image as background-image in fluid"
date:   2016-03-24 10:22:55 +0100
author: "Birger Fühne"
tags: 
  - TYPO3
  - Snippet
---
<p class="lorem">Sometimes, I just can't seem to remember certain snippets, although I use them quite regularly (which makes me think age might be getting the better of me). Aside the old doctype declaration, this is the use of fluid shorthand notations, especially when it comes to the use of an image as a background-image</p>
<p>
	So here's the code piece, for me to copy and paste again and again:
</p>
<p>
{% highlight json linenos=table %}
{f:uri.image(src:mediaElement.uid, treatIdAsReference:1)}
{% endhighlight %}	
</p>