---
layout: post
title:  "Displaying arrows with simple SVG"
date:   2016-03-24 18:12:25 +0100
author: "Birger Fühne"
tags: 
  - SVG
---

<p class="lorem">When I need some simple arrows, it's always a hassle to fire up Illustrator just to create that simple little icon. But no more a little gem that makes creating an arrow swift and easy.</p>
<!--more-->

<p class="typl8-drop-cap">
	Here's the full svg code for the arrow and it's head. The arrowhead is defined independently, so you can easily create something like double-headed arrows. For more awesome SVG arrow action, check the <a href="http://thenewcode.com/1068/Making-Arrows-in-SVG">full article at thenewcode.com</a>
</p>

*The following is not exactly the original code, as I noticed a slight gap between the line and the arrowhead, which I fixed by altering the refX of the marker#arrowhead:*

{% highlight svg linenos=table %}
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350 100">
  <defs>
    <marker id="arrowhead" markerWidth="10" markerHeight="7" 
    refX="1" refY="3.5" orient="auto">
      <polygon points="0 0, 10 3.5, 0 7" />
    </marker>
  </defs>
  <line x1="0" y1="50" x2="250" y2="50" stroke="#000" 
  stroke-width="8" marker-end="url(#arrowhead)" />
</svg>
{% endhighlight %}

<p>This will render in the browser like this:</p>

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350 100">
  <defs>
    <marker id="arrowhead" markerWidth="10" markerHeight="7" 
    refX="1" refY="3.5" orient="auto">
      <polygon points="0 0, 10 3.5, 0 7" />
    </marker>
  </defs>
  <line x1="10" y1="50" x2="250" y2="50" stroke="#000" 
  stroke-width="8" marker-end="url(#arrowhead)" />
</svg>
