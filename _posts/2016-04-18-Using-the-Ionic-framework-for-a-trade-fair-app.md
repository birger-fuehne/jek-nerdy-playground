---
layout: post
title:  "Using the Ionic framework for a trade fair app"
date:   2016-03-29 09:26:55 +0100
author: "Birger Fühne"
tags: 
  - Ionic
  - Hybrid-App
---

I'll give you a brief tour of how I used the Ionic framework to creae my first app - a rather simple app used as a product display for a trade fair. It is not a step-by-step tutorial, but I'll show you the main features requested and how I went about implementing them.

<!--more-->
<p class="typl8-drop-cap">
I 've been dabbling around hybrid app development for quite some time now, at first with Sencha touch, after I build some small JavaScript application with Ext JS. But during the process, it turned out that Sencha Touch/EXT Js was not really a product/company wanted to bet on:</p>

+ There's no proper issue tracking. You have to use some forum software where you constantly wonder whether support has read your bug thread yet.
+ There were *a lot* of bugs in core components, especially data stores, which did not get fixed.
+ Although core components were not fixed, they issued new major version making you pay again, even when those old bugs were not fixed.

Together with bad rendering performance for minor amounts of data (which may be my fault as well), I've decided to step away from these Sencha products (coincidentally, that's what TYPO3 is doing as well for some time now).

## Discovering Ionic

But somehow, I kept my eyes open for other, similar solutions. Of all the tools and frameworks I've come across, the <a href="http://ionicframework.com/">Ionic framework</a> really looked promising, even though learning AngularJS put up another hurdle to take. 

Thanks to the awesome community, blogs, and online courses, I put together various little demos and tests. Just when I felt not completely stupid anymore, our 3D Artist came along with a client project. He was working on a fair stand, where the client would like to have several iPads to be used by the visitors. The idea was that when all service personal would be busy, the clients could check out the products on their own using iPad Pros. And my colleague asked me whether that would be possible at all - and it was.

This proved to be the ideal starting point for a first real app - the usage situation was controlled, the scope very narrow, and, best of all, I didn't have to wrestle with the Apple review process. 

## The trade fair app

Unfortunately, I can't share screenshots of the app itself, but the app and its interface is quickly described:

- a start screen, displaying 6 products as SVG in a masonry-like fashion
- on interaction, the SVGs would scale and morph to center of the screen, eventually filling it up completely
- then fade to a detail page, where the product with some data and an image carousel would be shown.
- the detail page as a bottom bar linking to the other products
- If no interaction happend for x seconds, fade back to the start screen

I assume you are already somewhat familiar with Ionic, meaning you know how to setup a simple blank project and how to compile it for use with the xcode simulator or installing it on your device. I'm now describing the aspects of the main features previously mentioned in that bullet list.

### The startup screen
From the designer, I was handed some graphic showing the startup screen and the app icon (although at the fair, the visitors would never see them as they were locked in guided mode). With the great Ionic command line tool, creating the required images and icons just needs two steps - saving your inital files in the /resources direcory, and then running <code>$ ionic resources</code>. The rest is magic, and leaves you with the appropriate icons for the different devices and form factors.

The template of the "homepage" basically consists of six boxes, layed out using the amazing flexbox. The animation is also done via css, and triggered by class change on the item. The basic structure of the template for the start screen looks something like this:

{% highlight html linenos=table %}
<ion-view view-title="Amazing trade fair app">
  <ion-content>
  	<div class="Boxlinks">
  		<a class="Boxlink Boxlink__1"></a>
  		<a class="Boxlink Boxlink__2"></a>
  		..
  		<a class="Boxlink Boxlink__6"></a>
  	</div>
  </ion-content>
</ion-view>
{% endhighlight %}

Now we just add some interaction handling on these links to determine which animation we need to trigger, and which product to display afterwards. So this is what the html source look for one Boxlink:

{% highlight html linenos=table %}
<a class="Boxlink Boxlink_1" ng-class="{active : isSelected('tab.product-1')}" ng-click="setAnimatedSection('tab.product-1')">
  <span class="Boxlink-cta">
    <img src="images/products/product-1.svg" alt="">
  </span>
</a>
{% endhighlight %}

<small>Notice that for the product image I use a .svg. There was quite some scaling going on, and using bitmaps on that iPad Pro screen just looked terrible. Even using .svg files looked terrible at first, because I used the wrong approach on scaling the images: I started off from small images in the grid view, which then got scaled by a factor of ~3, producing a lot of artefacts (yes, on a .svg). Turns out the scaling on the iPad is done internally as bitmap as well. The workaround is to turn the process upside down: Start from the biggest view (image is as big as it will ever get), and then scale it *down* to the required size in the grid view. That really took some time to figure out, hope I can save you some troubles there.</small>

Back to determining the right section to apply. For the home screen I added a Controller to recognize and digest interactions with the Boxlinks using the state provider within the config (app.js):

{% highlight javascript linenos=table %}
.config(function($stateProvider, $urlRouterProvider) {
  .state('home', {
    cache: false,
  	url: '/',
  	templateUrl: 'templates/home.html',
  	controller: 'LinkCtrl'
  })
  [..]
{% endhighlight %}

And the LinkCtrl is pretty straightforward as well (controllers.js):

{% highlight javascript linenos=table %}
.controller('LinkCtrl', function($scope, $state, $timeout) {
  $scope.$on('$ionicView.enter', function(e) {
    $scope.selected = null;
  });

  $scope.isSelected = function(section){
     return $scope.selected === section;
  }
  $scope.setAnimatedSection = function(section){
    $scope.selected = section;
    $timeout(function(){
      $state.go(section);
    }, 1550);
    }
})
  [..]
{% endhighlight %}

This will set the class of the Boxlink in order to trigger the animation, and will change to the correct detail view after some time has elapsed. Yep, hard-coding the animation duration here in JavaScript and the CSS is not nice, but I have no better idea - enlighten me if you can!

So this is basically it for the start screen - positioning and animation is done using good old CSS, and triggering the animation and linking to the detail page is done using JavaScript.

### The detail view

The detail view with its bottom bar is realized using the tab component of the Ionic framework. Here's the template for the tabs component:

{% highlight html linenos=table %}
<ion-tabs class="tabs-color-active-positive">
  <!-- Product 1 -->
  <ion-tab title="Product 1" ui-sref="tab.product-1" ng-click="changeTab('tab.product-1')">
    <ion-nav-view name="tab-product-1"></ion-nav-view>
  </ion-tab>
  [..]
</ion-tabs>
{% endhighlight %}

If a tab is clicked, the ng-click attribute calls a function in the controller, passing the selected controller as string. The controller will apply a timeout for animation to happen, and then change the view to the selected tab:

{% highlight javascript linenos=table %}
.controller('TabCtrl', function($scope, $state, $timeout, $ionicPlatform, $interval, ActivityTracker, GestureRecognizer) {
  $scope.changeTab = function(stateAsString) {
    angular.element(document.getElementById("type-view"))
      .addClass("leaving");
    $timeout(function() {
      $state.go(stateAsString);
    }, 750);
  }
});
{% endhighlight %}
*Don't worry about all the parameters passed into the controller, I'll get to that in a bit.*

The class "leaving" triggers the CSS animation (some basic fadeout), and after 750ms the app changes to another tab by calling the <code>$state.go</code> function.

Now what happens when entering a new tab? Something very similar - apply some class to trigger another animation. Another feature get's activated here as well. Remember the feature request to return to home after some time of inactivity? That's implemented here as well, because if a tab has changed, it is the result of a user interaction. After that, I'll check for further interaction and measure the time until then:

{% highlight javascript linenos=table %}
$scope.$on('$ionicView.enter', function() {
  //init activityTracker and GestureRecognizer
  //activitytracker tracks time, the other one gesture events
  ActivityTracker.init($scope);
  ActivityTracker.updateTimestamp();
  GestureRecognizer.startLookingForGestures();

  //apply interval only once
  if($scope.returnHomeCheckIsRunning === false) {
    $scope.returnHomeCheckIsRunning = $interval(ActivityTracker.checkIfViewShouldReturnHome, 5000);
  }
		
  //start in/out transitions by adding classes to ion-content
  angular.element(document.getElementById("type-view"))
    .removeClass("afterEnter");
  angular.element(document.getElementById("type-view"))
    .removeClass("leaving");
});
{% endhighlight %}
*also part of the TabCtrl*

As you might see from the code already, entering the view starts two services - the activityTracker and the GestureRecognizer. The GestureRecognizer detects - yes, gestures. And will then notify the tracker that an interaction has happened. Every 5 seconds, the activityTracker is being asked if the view should go back home, and it will decide based on the time that passed since the last user interaction. 

The activityTracker and the gesture recognizer are implemented as services:

{% highlight javascript linenos=table %}
angular.module('starter.services', [])

.factory('ActivityTracker', function($state){
  var lastActivityTimeStamp = null;
  var timeToPassUntilHomeviewIsActive = 30000; //return home after 30 seconds
  var $scope;
  var service = {
    init: function(scope) {
      $scope = scope;
    },
    
    updateTimestamp: function() {
      lastActivityTimeStamp = new Date();
    },

    clearTimestamp: function() {
      lastActivityTimeStamp = null;
    },

    checkIfViewShouldReturnHome: function() {
      var currentDate = new Date();
      var timeBetweenNowAndLastGesture = currentDate - lastActivityTimeStamp;
      if( timeBetweenNowAndLastGesture > timeToPassUntilHomeviewIsActive) {
        $state.go('home');
      }
    }
  }
  return service;
})
{% endhighlight %}
*The ActivityTracker, controlling timestamps and their differences*

And finally, the GestureRecognizer:

{% highlight javascript linenos=table %}
.factory('GestureRecognizer', function($ionicPlatform, $ionicGesture, ActivityTracker){
  var recognizer = {
    startLookingForGestures: function($scope) {
      var element = angular.element(document.getElementById("eventPlaceholder"));
      var events = [
        { event: 'swipe'}
      ];

      angular.forEach(events, function(obj){
        $ionicGesture.on(obj.event, function (event) {
          ActivityTracker.updateTimestamp();
        }, element);
      });
    }
  }
  return recognizer;
})
{% endhighlight %}

If a swipe is detected, the ActivityTracker is asked to update its timestamp of last interaction.  used swipe, because the detail view consists of a swipable gallery. You could attach other events here as well. As you can see, the GestureRecognizer is based on Ionics <code>$ionicGesture</code> service.

The whole app development process, including optimizing SVGs from a CAD export, doing the initial design, prototyping, testing, and finally deploying on to the several iPad Pros took roughly four days. Thanks to the high quality of the Ionic framework, an active community and a whole lot of high quality online resources, the whole process went really smooth. In case you just started out on your journey learning Ionic, I'll share my most used resources with you:

### Resources learning Ionic
I might be more of an old-school type of person, but I really like having a physically book if I know that I'll be spending some serious time on a topic. For Ionic, I can recommend <a href="https://www.manning.com/books/ionic-in-action">Ionic in Action</a> and the <a href="http://www.amazon.com/Ionic-Cookbook-Hoc-Phan/dp/1785287974/ref=sr_1_sc_1?s=books&ie=UTF8&qid=1461046732&sr=1-1-spell&keywords=ionic+cookbock">Ionic Cookbok</a>.

If you are looking for online resources, I highly recommend <a href="https://devdactic.com/tag/ionic/">Devdactic</a>, there are many hands-on examples for integrating services like datasources, backend-as-a-service like FireBase or Backand etc. You should also check out the blog of <a href="https://www.raymondcamden.com/search/?q=ionic">Raymond Camden</a>, many useful tutorials and articles, for Ionic and web development in general. Similarly, <a href="http://www.joshmorony.com/">Josh Morony</a>
created an extensive collection of articles and tutorials, definitley worth a visit.

In short - the internet is awesome, because there are awesome people sharing their experience and knowledge - often for free. A-ma-zing!
