---
layout: post
title:  "My workflow using Grunt and TYPO3"
date:   2016-03-28 20:22:55 +0100
author: "Birger Fühne"
tags: 
  - TYPO3
  - Grunt
---

<p class="typl8-drop-cap">A recent topic in the TYPO3 chats I had was the overall project setup and workflow, a topic which I think is a bit neglected. Sure, everybody has its own workflow, but it's nice to have a look at how other people organize their work, as it's something not apparent from the end result.</p>
<!--more-->

## The prototype setup

First of all, of course, you want to have a versioning system of some flavour. I've been a long term SVN user, but too be honest, I always felt it too be a bit cumbersome in the general workflow. Nowadays I've switched to Git, and use <a href="http://bitbucket.org">bitbucket</a> as central hub. The nice thing about Bitbucket is its pricing model, as you can have as many private repositories as you'd like - the pricing is depending on the number of users. If you'd like to have public repositories by default with many different users, <a href="http://www.github.com">Github</a> might suit you better as it bases the pricing private repositories.

Using Bitbucket, you can define projects, and inside these projects you can define repositories. And that's exactly how I setup the dev workflow in our agency. For each project, we create two repositories in two different folders, which also serve as document roots in the apache configuration.

One repository/folder is used for the prototype we are developing during the project. After the first look and feel is created in Photoshop (yes, that's where we start), we start developing the prototype. The prototype can vary in its amount and detail, but is always the foundation for the integration in TYPO3.

We use several libraries and frameworks as a basis of our prototype:

+ Twig
+ SCSS
+ Grunt
+ etc.

Twig as templating engine is really awesome and flexible, and you start to  divide the page contents into modules (if that hasn't been done in the PSD already - there is no perfect world.). And you also get the ususal benefits - DRYness, reusable components, etc. Twig as widely used templating engine also makes work with other parties easier, e.g. in case you contract ends with the development of the prototype, and the integration to whatever CMS is done by a 3rd party.

The JavaScript and CSS work is encapsulated using a preprocessor, in our case that's Grunt. Yes, I've had a look at Gulp as well, but in our projects we had no real performance issues regarding compile time (hooray for <a href="https://github.com/sass/libsass">LibSass</a>). Depending on the type and scope and of the project, some framework (foundation being my favourite) or grid system (I highly recommend <a href="http://susy.oddbird.net/">Susy</a>) might be added to the project. Thanks to the awesome <a href="https://github.com/postcss/postcss">PostCSS</a> and it's plugins, the CSS is then optimized in various ways.

The javascript is handled similarly, I highly recommend linting your code even in small projects, it really saves your butt and in the end will save you a lot of time.

All the CSS and JS is placed within a dist/ folder in side the prototype folder, depending on Grunt options it may be minified/concatenated etc.

## The TYPO3 setup

I have adopted the habit to provide the general website setup of our [TYPO3 projects as an extension]({% post_url 2016-04-14-Using-a-TYPO3-provider-extension %}), thus not use a bunch of files/folders within the fileadmin/ directory, and you should do so, too! This extension is an own repository within the project's bitbucket project.

Assuming the template extension resides in a folder template_ext, you'd have the following folder structure:

```
├── template_ext/
[...]
│   ├── Resources
│       ├── Private
│           ├── Layouts
│           ├── Partials
│           ├── Templates
│       ├── Public
│           ├── Javascripts
│           ├── Stylesheets
```

Now how do you transfer the resulting .css and .js from your prototype to the TYPO3 installation? You definitely don't want that to be a manual process, but luckily there's a Grunt plugin doing what you need, <a href="https://github.com/gruntjs/grunt-contrib-copy">grunt-contrib-copy</a>. In my Gruntfile, there's a "copy" task that will copy the resulting frontend files to the appropriate folders of the TYPO3 extension. This copy task is part of the tasks that are being executed as part of the "watch" task.

The overall goal for me is that the prototype is the source of truth - the way the website works in the prototype must be the way it's working with the CMS attached. Any change on the frontend code must me done (and tested) in the prototype. For ease of deployment, I include the .css and .js files in the template_ext within that repository as well, although that somehow feels a bit dirty (but has proven to be helpful several times).

That's my workflow in a nutshell - I'd love to see how other people are organizing their workflow from prototype to CMS (Just as I'd like to see how other people are setting up their TYPO3 backends, also something that's not often shared).