---
layout: post
title:  "Custom content elements with gridelements"
date:   2016-03-25 19:26:55 +0100
author: "Birger Fühne"
tags: 
  - TYPO3
---

<p class="typl8-drop-cap">One thing that nagged me (and apparently also a lot of other people) was the lack of an easy way to create custom content elements - easy meaning without developing an extension for it, and with the possibility to nest content elements. Enter gridelements, which has made all of this incredibly easy and straight forward. Here's how I have integrated gridelements into my workflow.</p>
<!--more-->

<p><small>The following reflects the workflow for TYPO3 6.2 and css_styled_content. Although the principle stays the same in v7, I highly recommend taking a look at the newly introduced fluid_styled_content (coming up in another post soon&trade;).</small></p>

With gridelements coming available, many problems of creating nested and custom conent elements were eliminated. That made it very easy and intuitive to combine differently sized grid elements and make them available to the editor. This allowed flexible column elements, like alternating one full-width element and another two column section.

And that's what we are going to do here - create a two-column content element to enable editors to alternate side-by-side and full-width content. The general idea is that by default, content will span the entire available width, and the two-column layout splits this full width in half (50%/50%).

## Defining the gridelement container for the two column layout

I'll assume you already know how to install gridelements - install it in the extension manager, and don't forget to include it in your template!

Next, create your gridelement in the backend with a 2-column structure. . The alias will be used to hook up the TypoScript configuration. The element itself should look like this:

<figure>
  <img src="/assets/gridelements-two-column/two-column-gridelement.png" alt="">
  <figcaption>
    <small>Notice the column numbers. These will be used later in our TypoScript.</small>
    <p>
    This is the definition. I have not limited the allowed gridtypes or content elements, this is something you should adjust to your project. Limiting the choice of content elements will make the job of the editor a lot easier - less thinking, less room for error (but also less flexibility).</p>
  </figcaption>
</figure>

Here's the TypoScript setup for the gridelement. For the sake of DRYness, I have split the rendering definition of the gridelement itself and the definition of the rendering of the nested content elements. Using the CType, you could easily apply special rendering definitions for different CTypes - e.g. limiting max sizes of images for responsive image rendering. By default, content elements will be rendered as they would if they were outside any gridelement. Be sure that the alias of the gridelement used in the backend is the same as in your TypoScript setup - in this case it's "twocolumn".

{% highlight php linenos=table %}
# Define how content elements inside our two column content is rendered
temp.columnrendering = CASE 
temp.columnrendering {
  key.field = CType
  default =< tt_content
}

# Define the two column grid element setup
lib.gridelements {
  twocolumn < .defaultGridSetup
  twocolumn {
    wrap = <div class="TwoColumnContainer">|</div>
    columns.0 {
      wrap = <div class="TwoColumnContainer__column TwoColumnContainer__column--left">|</div>
      renderObj < temp.columnrendering
    }
    columns.1 {
    wrap = <div class="TwoColumnContainer__column TwoColumnContainer__column--right">|</div>
        renderObj < temp.columnrendering
    }        
  }
}
tt_content.gridelements_pi1.20.10.setup.twocolumn < lib.gridelements.twocolumn
{% endhighlight %}

The backend should look something like this:
<figure>
  <img src="/assets/gridelements-two-column/two-column-gridelement-backend.png" alt="">
  <figcaption>
    The backend view of our two column gridelement after it has been inserted on a page.
  </figcaption>
</figure>

Now our beloved editors can easily place content side-by-side and have a visual cue in the backend of how the content is being displayed in the frontend.

I think that this example gives a good start into structuring your content with gridelements - but this is only a short introduction into gridelements. If you like this approach, be sure to delve deeper into concepts like defining your setup in external files so you can integrate it into your versioning system of choice. You should also have look at the great examples in the <a href="https://docs.typo3.org/typo3cms/extensions/gridelements/">Gridelements manual</a>.

In one of the next posts, I'll explain how to add some flexform configuration to your gridelement. Using flexform, we can enhance the editor's flexibility by allowing him to choose from a number of column ratios (50-50, 25-75, 75-25) (and why you should never ever place content there).