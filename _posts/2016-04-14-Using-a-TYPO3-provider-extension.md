---
layout: post
title:  "Setting up a provider extension for TYPO3"
date:   2016-03-23 09:26:55 +0100
author: "Birger Fühne"
tags: 
  - TYPO3
---

<p class="lorem">The best way to setup your TYPO3 is using a provider extension. A provider extension will 
host all the configuration details and helps to separate userland and your configuration files.</p>
<!--more-->

<p><small>In the following, I assume that you are already familiar with basic TYPO3 setup like configuring the page type, add a site root and it's template, etc. If not, you should make yourself familiar with templating using TypoScript and fluid.</small></p>

<p class="typl8-drop-cap">
If you have been storing your configuration in some folder like <code>/fileadmin/template/</code>, it's time to learn a new way to configure your TYPO3 installation - a provider extension (or site extension).
</p>

A provider extension is technically not different from any other extension you have already used. It's located in the same location as any other extension - in typo3conf/ext/extension. Setting one up is very easy - you can do it manually or you can use the extension "extension_builder" to create an extension sceleton.

If you do it manually, go ahead and create a new folder within /typo3conf/ext/. The folder name should be written in lower case letters, and it is also used as your extension name. Separate words in the folder name by an underscore; "My Beautiful Template" would become <code>typo3conf/ext/my_beautiful_template</code>.

<blockquote>
	The TYPO3 coding guidelines demand that the name of an extension is always written in UpperCamelCase (beginning with a capital letter, then upper and lower letters; no underscore), while the extension key may only contain small letters and underscore.	
	<p>
	<small><a href="https://docs.typo3.org/typo3cms/ExtbaseFluidBook/a-CodingGuidelines/Index.html">View coding guidelines</a></small>
	</p>
</blockquote>

Inside this folder a few files are mandatory for TYPO3 to recognize your extension as such. First and foremost, an "ext_emconf.php" is needed, so the extension manager is able to recognize the extension and its dependencies. 

A sample file could look like this:

{% highlight php linenos=table %}
<?php
$EM_CONF[$_EXTKEY] = array(
	'title' => 'My beautiful template',
	'description' => 'A website template using an extension',
	'category' => 'misc',
	'author' => 'Birger Fühne',
	'author_company' => '',
	'author_email' => 'birger.fuehne@gmail.com',
	'clearCacheOnLoad' => 1,
	'version' => '0.0.0',
	'state' => 'stable',
	'clearcacheonload' => 1,
	'constraints' => array(
		'depends' => array(
			'typo3' => '7.6.0-7.6.99',
			'extbase' => '7.6.0-7.6.99',
			'fluid' => '7.6.0-7.6.99',
		),
	)
);
?>
{% endhighlight %}

This provides the extension manager with basic information about your extension, like the title and description, which appear at various places in the backend, like the extension listing within the extension manager. It also enables TYPO3 to check for dependencies and conflicts - preventing the installation in incompatible setups. If, for example, at one point you are using other extensions like gridelements, the dependencies ensure that you have gridelements installed before your own extension can be used.

This is already all you need to configure an extension and install it via the extension manager - albeit it doesn't do much yet. As you might recall from your previous setups, TYPO3 expects some constants and setup files, instructing TYPO3 where to look for templates, stylesheets, javascripts, language configuration, etc. 

The aforementioned [coding guidelines][coding-guidelines] provide an overview of the usually needed folders and files, which you should create accordingly in your extension folder. Afterwards, your folder structure should something look like this:

```
├── my_beautiful_template/
│   ext_emconf.php
│   ├── Configuration
│   	├── TypoScripts
│   	constants.txt
│   	setup.txt
│   ├── Documentation
│   ├── Resources
│       ├── Private
│           ├── Layouts
│           ├── Partials
│           ├── Templates
│       ├── Public
│           ├── Javascripts
│           ├── Stylesheets
```
	
That's nice and all, but it does not do very much up until now, because TYPO3 won't know about your configuration that you have done in your <code>setup.txt</code> or <code>constants.txt</code>. In order to make your configuration available, place a file called <code>ext_tables.php</code> in the same folder as your <code>ext_emconf.php</code>.

{% highlight php linenos=table %}
<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
	$_EXTKEY, 'Configuration/TypoScript',
	'My beautiful template'
);
?>
{% endhighlight %}

This will add your configuration to the static templates section in your TYPO3 backend, where you need to include it in your template configuration. Add some basic TypoScript to your setup.txt to see if everything works as expected:

{% highlight txt linenos=table %}
page = PAGE
page {
	typeNum = 0
	10 = TEXT
	10.value = Hello World
}
{% endhighlight %}

Now you can continue setting up your website configuration in the same way you would have done it before, but now you have nicely redistributable extension that can also check for conflicts and dependencies. You can further customize it by adding an <code>ext_icon.gif</code>, override some TCA, extend other extensions, and so on. In a follow-up, I'll show you the basic setup I use as a basis for my projects.

[coding-guidelines]: https://docs.typo3.org/typo3cms/ExtbaseFluidBook/a-CodingGuidelines/Index.html
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
