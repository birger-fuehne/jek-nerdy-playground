---
layout: page
title: Impressum
permalink: /Impressum
---

<p>
	<strong>Angaben gemäß § 5 TMG:</strong><br>
	Birger Fühne<br>
	Ziegeleistr. 22A<br> 
	49134 Wallenhorst <br>
</p>
<p>
	<strong>Kontakt:</strong><br>
	<a href="mailto:wwwmynerdyplayground@gmail.com">wwwmynerdyplayground@gmail.com</a>
</p>
<p>
	<strong>Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV:</strong><br>
	Birger Fühne
</p>

<h2>Credits</h2>
<p>
	Favicon taken from  <a href="https://icons8.com/">https://icons8.com</a>
</p>
